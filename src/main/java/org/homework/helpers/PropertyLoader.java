package org.homework.helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import static java.lang.System.getProperty;

public class PropertyLoader {

    private final static Logger LOG = LogManager.getLogger("Properties helper");

    public static void writeAllureEnv(String name, String value) {
        Properties props = new Properties();
        File environment = new File(getProperty("user.dir")
                                            + "/src/test/resources/environment.properties");
        try (InputStream input = new FileInputStream(environment.getPath())) {
            props.load(input);
        } catch (IOException e) {
            LOG.error(e);
        }
        props.setProperty(name, value);
        try (OutputStream out = new FileOutputStream(environment.getPath())) {
            props.store(out, "Updated: "
                    + new SimpleDateFormat("HH:mm:ss dd.MM.yyyy").format(new Date()));
        } catch (IOException e) {
            LOG.error(e);
        }

    }
}
