package org.homework.model;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.AbstractDriverOptions;

import java.util.logging.Level;

public enum BrowserConfig {

    CHROME("chrome", "115.0"),
    FIREFOX("firefox", "113.0"),
    EDGE("edge", "114.0");

    BrowserConfig(String name,
            String version) {
        this.name = name;
        this.version = version;
        this.capabilities = new MutableCapabilities();
    }

    private final String name;
    private final String version;

    private AbstractDriverOptions driverOptions;
    private final MutableCapabilities capabilities;

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public AbstractDriverOptions getDriverOptions() {
        return driverOptions;
    }

    public MutableCapabilities getCapabilities() {
        if(this.driverOptions != null) {
            String capabilityName;
            switch(this) {
                case FIREFOX -> {
                    return this.driverOptions;
                }
                case EDGE -> capabilityName = EdgeOptions.CAPABILITY;
                default -> capabilityName = ChromeOptions.CAPABILITY;
            }
            this.capabilities.setCapability(capabilityName, this.driverOptions);
        }
        return capabilities;
    }

    public BrowserConfig setOptions(@NonNull String... options) {
        AbstractDriverOptions innerOptions;
        switch(this) {
            case FIREFOX -> {
                innerOptions = new FirefoxOptions();
                ((FirefoxOptions) innerOptions).addArguments(options);
            }
            case EDGE -> {
                innerOptions = new EdgeOptions();
                ((EdgeOptions) innerOptions).addArguments(options);
            }
            default -> {
                innerOptions = new ChromeOptions();
                ((ChromeOptions) innerOptions).addArguments(options);
            }
        }
        if(this.driverOptions == null) {
            this.driverOptions = innerOptions;
        } else {
            this.driverOptions.merge(innerOptions);
        }
        return this;
    }

    public BrowserConfig enableIncognito(boolean enable) {
        if(enable) {
            switch(this) {
                case CHROME -> {
                    return this.setOptions("--incognito");
                }
                case EDGE -> {
                    return this.setOptions("inprivate");
                }
                case FIREFOX -> {
                    return this.setOptions("-private");
                }
            }
        }
        return this;
    }

    public BrowserConfig enablePerformanceLog(boolean enable){
        if (enable){
            LoggingPreferences logPrefs = new LoggingPreferences();
            logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
            logPrefs.enable(LogType.BROWSER, Level.ALL);
            logPrefs.enable(LogType.CLIENT, Level.ALL);
            logPrefs.enable(LogType.DRIVER, Level.ALL);
            switch (this){
                case FIREFOX -> this.driverOptions.setCapability("moz:loggingPrefs", logPrefs);
                case CHROME -> this.driverOptions.setCapability("goog:loggingPrefs", logPrefs);
                case EDGE -> this.driverOptions.setCapability("ms:loggingPrefs", logPrefs);
            }
        }
        return this;
    }

}
