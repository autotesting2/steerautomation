package org.homework.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.homework.pages.authorized.MainPage;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.SetValueOptions.withText;
import static com.codeborne.selenide.logevents.SelenideLogger.step;

public class LoginPage extends BasePage{

    //https://dev2frontend.ngrok.io/login
    public LoginPage() {
        super("SignIn Page");
    }

    private final SelenideElement loginField = $x("//input[@id='username']").as("loginField");
    private final SelenideElement passwordField = $x("//input[@id='password']").as("passwordField");
    private final SelenideElement submitButton = $x("//span[text()='Log In']/..").as("submitButton");
    private final SelenideElement errorMessage = $x("//span[text()='Email is not valid']")
            .as("errorMessage");

    @Step("Sign in SteerApplication")
    public MainPage signIn(String username, String password) {
        log.info("Trying to enter in application...");
        step("Filling auth fields", () -> {
            loginField.shouldBe(Condition.visible).setValue(withText(username).withDisplayedText("AQA hidden username"));
            passwordField.shouldBe(Condition.visible).setValue(withText(password).sensitive());
            submitButton.shouldBe(Condition.visible).click();
        });
        if(errorMessage.isDisplayed()){
            log.error("Authentication failure: {}", errorMessage.getText());
            return null;
        } else {
            log.info("Authentication success!");
            return new MainPage();
        }

    }

}
