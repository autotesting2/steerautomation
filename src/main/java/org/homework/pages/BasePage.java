package org.homework.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class BasePage {

    protected final Logger log;

    protected BasePage(String title) {
        this.log = LogManager.getLogger(title);
        log.info("Page \"{}\" opened", title);
    }
}
