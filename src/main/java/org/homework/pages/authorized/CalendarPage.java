package org.homework.pages.authorized;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.logevents.SelenideLogger.step;
import static java.lang.String.format;

public class CalendarPage extends AuthorizedPageMenu {
    protected CalendarPage() {
        super("Calendar Page");
    }

    private final SelenideElement dateForSearchInput =
            $x("//div[@class = 'react-datepicker__input-container']/input").as("dateForSearchInput");

    private SelenideElement timeInterval(String interval) {
        return $x(format("//div[text() = '%s']/../..//input", interval)).as("timeInterval " + interval);
    }

    private final SelenideElement eventInfo =
            $x("//div[contains(@class, 'eventDetails')]//div[contains(@class, 'tertiary')]")
                    .as("eventInfo block");

    private ElementsCollection eventInfo(String param) {
        return $$x(format("//div[contains(@class, 'eventInfo')]//span[contains(@class, '%s')]", param))
                .as("event list with param: " + param);
    }

    private final SelenideElement createNewAppointmentButton = $x("//button[contains(@class, 'button_primary')" +
            "]//span[text() = 'New Appointment']").as("createNewAppointmentButton");

    @Step("Open Appointment Creation Window from Calendar Page")
    public AppointmentCreationWindow openAppointmentCreationWindow() {
        log.info("Opening Appointment Creation Window...");
        step("Creating Appointment", () -> {
            createNewAppointmentButton.shouldBe(Condition.visible).click();
        });
        return new AppointmentCreationWindow();
    }

    @Step("Validating Appointment creation")
    public CalendarPage validateAppointment(String name,
                                            Month month,
                                            int dayOfMonth,
                                            int year,
                                            String timeInterval) {
        log.info("Validating results of Appointment Creation...");
        step("Switch calendar view from Month to Day", () -> {
            refresh();
            timeInterval("Month").shouldBe(Condition.visible).sendKeys("Day\n");
        });
        step("Filtering date", () -> {
            dateForSearchInput.shouldBe(Condition.visible).click();
            new DatePicker().pickDate(month.getDisplayName(TextStyle.FULL, Locale.US), dayOfMonth, year);
        });
        step("Searching results and validating", () -> {
            step("Looking for needed Appointment by Title", () -> {
                eventInfo("title")
                        .find(Condition.exactText(name))
                        .scrollIntoView(true)
                        .shouldBe(Condition.visible)
                        .click();
            });
            step("Validating opened Appointment", () -> {
                eventInfo.scrollIntoView(true)
                        .shouldBe(Condition.visible)
                        .shouldHave(Condition.exactText(format("%s %s, %s",
                                month.getDisplayName(TextStyle.FULL, Locale.ROOT), dayOfMonth, timeInterval)));
            });
        });
        log.info("Appointment is valid!");
        return this;
    }
}
