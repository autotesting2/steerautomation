package org.homework.pages.authorized;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.homework.pages.BasePage;
import org.homework.pages.LoginPage;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.logevents.SelenideLogger.step;

public abstract class AuthorizedPageMenu extends BasePage {

    protected AuthorizedPageMenu(String title) {
        super(title);
    }

    private final SelenideElement searchField =
            $x("//input[@type='text' and contains(@placeholder, 'Search by')]").as("searchField");

    private final SelenideElement addEntityButton =
            $x("//div[contains(@class, 'relative header-add-button')]").as("addEntityButton");
    private final SelenideElement addNewAppointmentButton = $x("//p[text() = 'New Appointment']")
            .as("addNewAppointmentButton");
    private final SelenideElement appointmentsButton = $x("//div[text() = 'Appointments']/../../..")
            .as("appointmentsButton");
    private final SelenideElement calendarButton = $x("//div[text() = 'Calendar']")
            .as("calendarButton");

    private final SelenideElement profileButton = $x("//div[contains(@class, 'ProfileControl')]")
            .as("profileButton");
    private final SelenideElement logOutButton = $x("//p[text() = 'Log Out']").as("logOutButton");

    @Step("Open Calendar Page")
    public CalendarPage openCalendar() {
        log.info("Opening Calendar Page...");
        step("Work with Appointment tab", () -> {
            appointmentsButton.shouldBe(Condition.visible).click();
            calendarButton.shouldBe(Condition.visible).click();
        });
        return new CalendarPage();
    }


    @Step("Open Appointment Creation Window by Add Button")
    public AppointmentCreationWindow createAppointmentByAddButton() {
        log.info("Creating Appointment...");
        step("Opening Appointment window", () -> {
            addEntityButton.shouldBe(Condition.visible).click();
            addNewAppointmentButton.shouldBe(Condition.visible).click();
        });
        return new AppointmentCreationWindow();
    }


    @Step("LogOut of application")
    public LoginPage logOut() {
        log.info("Logout from Application...");
        step("Logout by profile menu", () -> {
            profileButton.shouldBe(Condition.visible).click();
            logOutButton.shouldBe(Condition.visible).click();
        });
        log.info("Done!");
        return new LoginPage();
    }
}
