package org.homework.pages.authorized;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.homework.pages.BasePage;

import java.util.Objects;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.logevents.SelenideLogger.step;
import static java.lang.String.format;

public class AppointmentCreationWindow extends BasePage {

    //https://dev2frontend.ngrok.io/shop/2e473a6b-2b0d-4a04-b0eb-de9b581e9798/appointments/calendar
    protected AppointmentCreationWindow() {
        super("Create New Appointment");
    }

    private final SelenideElement titleField = $x("//input[@name = 'title']").as("titleField");

    private SelenideElement typeButton(String type) {
        return $x(format("//div[text()='%s']", type)).as("typeButton with type: " + type);
    }

    private final SelenideElement customerField = $x("//div[text()='Customer']/../../..//div[@class = 'flex " +
            "items-center w-full']").as("customerField");
    private final SelenideElement statusField =
            $x("//div[text()='Status']/../../..//div[@class = 'flex items-center w-full']")
                    .as("statusField");
    //    private final SelenideElement datePickerField = $x("//div[@class= 'react-datepicker__input-container']/input");
    private final SelenideElement datePickerField =
            $$x("//div[@class= 'react-datepicker__input-container']/input").get(1)
                    .as("datePickerField");
    private final SelenideElement detailsField = $x("//textarea[@name= 'details']")
            .as("detailsField");
    private final SelenideElement createButton = $x("//span[text()= 'Create']/..")
            .as("createButton");
    private final SelenideElement cancelButton = $x("//span[text()= 'Cancel']/..")
            .as("cancelButton");


    @Step("Creating appointment: {title}")
    public CalendarPage createAppointment(String title,
                                          String type,
                                          String customer,
                                          String status,
                                          String month,
                                          int dayOfMonthFrom,
                                          int dayOfMonthTo,
                                          int year,
                                          String timeFrom,
                                          String timeTo,
                                          String details) {
        step("Filling values for Appointment creation", () -> {
            try {
                titleField.shouldBe(Condition.visible).sendKeys(title);
                typeButton(type).shouldBe(Condition.visible).click();
                createButton.scrollIntoView(true);
                if (!Objects.equals(customer, "")) {
                    customerField.shouldBe(Condition.visible).selectOption(customer);
                }
                if (!Objects.equals(status, "")) {
                    statusField.shouldBe(Condition.visible).selectOption(status);
                }
                datePickerField.shouldBe(Condition.visible).hover().click();
                new DatePicker().pickDateTime(month, dayOfMonthFrom, dayOfMonthTo, year, timeFrom, timeTo);
                titleField.scrollIntoView(true).click();
                detailsField.shouldBe(Condition.visible).sendKeys(details);
                createButton.scrollIntoView(true).shouldBe(Condition.visible).click();
            } catch (Throwable t) {
                cancelButton.scrollIntoView(true).shouldBe(Condition.visible).click();
                throw t;
            }
        });
        return new CalendarPage();
    }

}
