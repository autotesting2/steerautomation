package org.homework.pages.authorized;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.logevents.SelenideLogger.step;
import static java.lang.String.format;

public class DatePicker {

    private final SelenideElement inputTimeFromField = $$x("//div[contains(@class, 'timeInput')]//input")
            .get(0).as("inputTimeFromField");
    private final SelenideElement inputTimeToField = $$x("//div[contains(@class, 'timeInput')]//input")
            .get(1).as("inputTimeToField");

    private ElementsCollection datePickButton(String month, int dayOfMonth, int year) {
        return $$x(format("//div[contains(@aria-label, '%s %s') and contains(@aria-label, '%s')]", month,
                         dayOfMonth, year)).as(format("datePickButton of %s %s, %s", month, dayOfMonth, year));
    }

    private SelenideElement timePickButton(String time) {
        return $x(format("//li[text() = '%s']", time)).as(time + " selecting option");
    }

    @Step("Input date-time data in DataPicker")
    public void pickDateTime(String month,
                             int dayOfMonthFrom,
                             int dayOfMonthTo,
                             int year,
                             String timeFrom,
                             String timeTo) {
        step("Choosing date", () -> {
            datePickButton(month, dayOfMonthFrom, year).get(1).shouldBe(Condition.visible).click();
            datePickButton(month, dayOfMonthTo, year).get(0).shouldBe(Condition.visible).click();
        });
        step("Choosing time", () -> {
            inputTimeFromField.shouldBe(Condition.visible).click();
            timePickButton(timeFrom).scrollIntoView(true).shouldBe(Condition.visible).click();
            inputTimeToField.shouldBe(Condition.visible).click();
            timePickButton(timeTo).scrollIntoView(true).shouldBe(Condition.visible).click();
        });
    }

    @Step("Input date data in DataPicker")
    public void pickDate(String month,
                         int dayOfMonth,
                         int year) {
        step("Filtering date", () -> {
            datePickButton(month, dayOfMonth, year).get(1).shouldBe(Condition.visible).click();
        });
    }

}
