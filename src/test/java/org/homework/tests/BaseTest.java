package org.homework.tests;

import com.codeborne.selenide.logevents.SelenideLogger;
import com.codeborne.selenide.testng.TextReport;
import io.qameta.allure.selenide.AllureSelenide;
import io.qameta.allure.selenide.LogType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.homework.model.BrowserConfig;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Locale;
import java.util.logging.Level;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.Selenide.open;
import static java.lang.System.getProperty;
import static com.codeborne.selenide.Configuration.*;
import static org.apache.commons.io.FileUtils.copyFileToDirectory;
import static org.homework.helpers.PropertyLoader.writeAllureEnv;

@Listeners({TextReport.class})
public abstract class BaseTest {

    protected final static Logger LOG = LogManager.getLogger(BaseTest.class);

    private final BrowserConfig browserConf = BrowserConfig.valueOf(getProperty("browser", "chrome")
                    .toUpperCase(Locale.ROOT))
            .enableIncognito(true)
            .enablePerformanceLog(true)
            .setOptions("--disable-gpu", "--no-sandbox")
            .setOptions("--remote-allow-origins=*");

    @BeforeSuite(alwaysRun = true, description = "Suite initialization")
    public void initSuite() {
        System.setProperty("webdriver.http.factory", "jdk-http-client");

        baseUrl = "https://dev2frontend.ngrok.io";

        browser = browserConf.getName();
        browserVersion = browserConf.getVersion();
        timeout = Duration.ofSeconds(15).toMillis();
        pageLoadTimeout = Duration.ofSeconds(120).toMillis();
        browserSize = "1920x1080";
        browserPosition = "10x10";
        browserCapabilities = browserConf.getCapabilities();
        webdriverLogsEnabled = true;

        reportsFolder = "build/reports";
        downloadsFolder = "build/reports/downloads";
    }

    @BeforeMethod(alwaysRun = true, description = "Enabling AllureSelenide Listener")
    public void setAllureListener() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .savePageSource(true)
                .screenshots(true)
//                .enableLogs(LogType.PERFORMANCE, Level.ALL)
                .enableLogs(LogType.BROWSER, Level.ALL)
                .enableLogs(LogType.CLIENT, Level.ALL)
                .enableLogs(LogType.DRIVER, Level.ALL)
                .includeSelenideSteps(true));
    }

    @BeforeMethod(alwaysRun = true,
            dependsOnMethods = "setAllureListener",
            description = "Writing environment variables to report")
    public void writeCapsToAllure() {
        writeAllureEnv("browser.capabilities", browserConf.getCapabilities().toString());
    }

    @BeforeMethod(alwaysRun = true,
            dependsOnMethods = "writeCapsToAllure",
            description = "Opening Browser")
    public void initTest() {
        open("");
    }

    @AfterMethod(alwaysRun = true, description = "Closing Browser")
    public void tearDownTest() {
        closeWebDriver();
    }

    @AfterMethod(alwaysRun = true,
            dependsOnMethods = "tearDownTest",
            description = "Copy environment variables to report")
    public void copyAllureFiles() {
        try {
            copyFileToDirectory(new File(getProperty("user.dir")+ "/src/test/resources/categories.json"),
                    new File(getProperty("user.dir") + "/build/allure-results"));
            copyFileToDirectory(new File(getProperty("user.dir")+ "/src/test/resources/environment.properties"),
                    new File(getProperty("user.dir") + "/build/allure-results/"));
        } catch (IOException e) {
            LOG.error(e);
        }
    }
}
