package org.homework.tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.homework.pages.LoginPage;
import org.homework.pages.authorized.AuthorizedPageMenu;
import org.homework.pages.authorized.MainPage;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.TextStyle;
import java.util.Locale;

import static java.lang.Math.abs;
import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.util.Optional.ofNullable;

@Owner("Bohdan Kuso")
@Feature("Steer Appointments")
public class AppointmentCreationTest extends BaseTest {

    private MainPage page;
    private ZonedDateTime dateTime;

    @BeforeMethod(alwaysRun = true, description = "LogIn to Application")
    public void setUpAndLogIn() {
        this.dateTime = ZonedDateTime.now(ZoneId.of("Europe/Kyiv")).plusDays(1);
        this.page = new LoginPage()
                .signIn(getProperty("steer.username", "aqa.interview@mailinator.com"),
                        getProperty("steer.password", "P5vioN%jc&^b"));
    }

    @Story("Appointment Creation")
    @Test(description = "Check Appointment creation from Calendar Page")
    public void checkAppointmentCreationByCalendarPage() {
        final String name = "Test Appointment " + dateTime.getMinute();
        final int hourOfAppointment = abs(dateTime.getHour() + 1 - 12);
        ofNullable(this.page).ifPresentOrElse(
                page -> page.openCalendar()
                        .openAppointmentCreationWindow()
                        .createAppointment(name,
                                "Waiting",
                                "",
                                "",
                                dateTime.getMonth().getDisplayName(TextStyle.FULL, Locale.US),
                                dateTime.getDayOfMonth(),
                                dateTime.getDayOfMonth(),
                                dateTime.getYear(),
                                hourOfAppointment + ":00 PM",
                                hourOfAppointment + ":30 PM",
                                "")
                        .validateAppointment(name,
                                dateTime.getMonth(),
                                dateTime.getDayOfMonth(),
                                dateTime.getYear(),
                                format("%s:00 - %s:30pm", hourOfAppointment, hourOfAppointment)),
                () -> {
                    throw new SkipException("Authorization failure. Impossible to begin test. Skipped.");
                }
        );
    }

    @Story("Appointment Creation")
    @Test(description = "Check Appointment creation from Main Page")
    public void checkAppointmentCreationByMainPage() {
        final String name = "Test Appointment " + dateTime.getMinute();
        final int hourOfAppointment = abs(dateTime.getHour() + 1 - 12);
        ofNullable(this.page).ifPresentOrElse(
                page -> page.openCalendar()
                        .createAppointmentByAddButton()
                        .createAppointment(name,
                                "Waiting",
                                "",
                                "",
                                dateTime.getMonth().getDisplayName(TextStyle.FULL, Locale.US),
                                dateTime.getDayOfMonth(),
                                dateTime.getDayOfMonth(),
                                dateTime.getYear(),
                                hourOfAppointment + ":00 PM",
                                hourOfAppointment + ":30 PM",
                                "")
                        .validateAppointment(name,
                                dateTime.getMonth(),
                                dateTime.getDayOfMonth(),
                                dateTime.getYear(),
                                format("%s:00 - %s:30pm", hourOfAppointment, hourOfAppointment)),
                () -> {
                    throw new SkipException("Authorization failure. Impossible to begin test. Skipped.");
                }
        );
    }

    @AfterMethod(alwaysRun = true, description = "LogOut from Application")
    public void logOut() {
        ofNullable(this.page).ifPresent(AuthorizedPageMenu::logOut);
    }

}
