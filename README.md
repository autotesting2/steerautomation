[![pipeline status](https://gitlab.com/autotesting2/steerautomation/badges/main/pipeline.svg)](https://gitlab.com/autotesting2/steerautomation/-/commits/main)

### Steer Automation project for Senior Automation Position
Project for testing some web application. Using Java + TestNg + Selenide


### Getting started
1. First step for working with project is cloning [current](https://gitlab.com/autotesting2/steerautomation.git) repo.
Use `git clone https://gitlab.com/autotesting2/steerautomation.git` for it.
2. Open cloned project in IDE, for example in Intellij IDEA, and wait for project initialization.
3. You can launch tests in project from your IDE (press `RUN` in code part), or by executing command
```shell
gradle test
```
IMPORTANT: You can use inner gradle-wrapper for launching gradle tasks. For this run 
```shell
./gradlew test
```
You can also change default run params, adding console keys for to switch browser, and change username and password.
For doing this, launch:
```shell
gradle test -Dsteer.username=<needed username> \
            -Dsteer.password=<needed password> \
            -Dbrowser=<preferred browser>
```

### The project directory structure
There are base project structure for Selenium Automation Test project, which used here, looks like:
```Gherkin
src
  + main
    + java
      + org
        + homework
          + helpers                   Additional Helpers classes
          + pages                     PageObject classes
          + models                    Object classes for storing entities data
  + test
    + java
      + org
        + homework
          + tests                     Test classes 
    + resources                       Configuration files of additional instruments
```

### Writing tests
You can write your own tests, using TestNg Framework and Selenide library. For doing it well, use prepared PageObject
classes and *Test classes in this project as template for your tests. For example, if you want to create new Page class, 
you should to extend your class from `BasePage.java` class, and override it constructor method. And for creating new 
Test classes - you should extend it from `BaseTest.java`. 

### Reporting
This project creating 2 types of reports:
1. Allure Report: generating after test execution task by running command 
```shell
gradle allureReport
```
2. TestNg Report: generating in real time, with test task.

### CI/CD
This project using GitLab-CI pipeline configuration, described in [.gitlab-ci.yml](.gitlab-ci.yml) file.
After pipeline execution, in list of artifacts of build tou can find zip archive with all project reports.
